// ==UserScript==
// @name        ChatRoom
// @namespace   _test
// @include     https://yonatan.us/*
// @include     https://example.com/*
// @include     https://tinflix.win/*
// @require     https://code.jquery.com/jquery-1.11.3.js
// @require     https://code.jquery.com/ui/1.11.3/jquery-ui.js
// @require     https://raw.githubusercontent.com/ROMB/jquery-dialogextend/master/build/jquery.dialogextend.min.js

// @resource    jqUI_CSS  https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css
// @resource    IconSet1  http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/images/ui-icons_222222_256x240.png
// @resource    IconSet2  http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/images/ui-icons_454545_256x240.png

// @grant GM_log
// @grant       GM_addStyle
// @grant       GM_getResourceText
// @grant       GM_getResourceURL
// @grant       GM_xmlhttpRequest
// ==/UserScript==

const encoded_url = new TextEncoder().encode(window.location.href);
const encoded_url_buffer = await window.crypto.subtle.digest('SHA-256', encoded_url);
const hashed_url = Array.from(new Uint8Array(encoded_url_buffer)).map(b => b.toString(16).padStart(2, '0')).join('');
let biggestId = 0;

$("head").append (
    '<link '
  + 'href="//ajax.googleapis.com/ajax/libs/jqueryui/1.13.2/themes/smoothness/jquery-ui.css" '
  + 'rel="stylesheet" type="text/css">'
);

//window.jQuery310 = $.noConflict(true);

//--- Add our custom dialog using jQuery. Note the multi-line string syntax.
$("body").append  (
    '<div id="gmOverlayDialog" style="font-size: 16px; height:400px; position:relative; z-index: 1;"> \
         <div id="nameArea" style="z-index: 3; background-color: #fafffa;"> \
              <label for="nameTextBox"> <b>Nickname</b>: <input id="nameTextBox" style="font-size: 22px; margin: 0px 0px 0px 0px; z-index: 3;" size=16 value=Anonymous> \
         </div> \
         <div id="chatBox" style="display:flex;"> \
             <span id="chatBoxText" style="font-size: 22px; display:flex; flex-direction:column-reverse; width: 100%; height: 100%; bottom: 60px; position:absolute; z-index: -2;"> \
             </span>\
         </div>\
\
         <div id="chatInputBox" style="position:absolute; bottom:20px; width:94%; z-index: 3;">\
             <div id="test" style="" >\
             <input type="text" id="chatBoxInputText" style="width: 100%; background-color: #e1FFe1; border:none; font-size: 22px;" autofocus>\
             </div> \
         </div>\
     </div>\
    '
);

let overlay_div = $("#gmOverlayDialog");

//--- Activate the dialog.
let dialog = overlay_div.dialog ( {
    modal:      false,
    title:      "Local chat",
    minHeight: 400,
    minWidth: 500,
    zIndex:     999999,   //-- This number doesn't need to get any higher.
    position: { my: "right top", at: "right top", of: window }
} ).css({'background': '#fafffa'}).dialogExtend({
        "maximizable" : true,
        "collapsable" : true,
        "dblclick" : "maximize",
        "icons" : { "maximize" : "ui-icon-circle-plus",
          "collapse" : "ui-icon-circle-minus", }
      }).prev(".ui-dialog-titlebar").css({'background': '#fafffa'});



$("#chatBoxInputText").keyup(function(event) {
    if (event.keyCode === 13) {
        //addMessage("test", $("#chatBoxInputText").val());
        sendMessage('username goes here', $("#nameTextBox").val(), $("#chatBoxInputText").val());
        $("#chatBoxInputText").val("");
    }
});

function addMessage(nick, content) {
    let newMessage = document.createElement("div");
    let newNick = document.createElement("span");
    let newText = document.createElement("span");
    newNick.innerHTML = nick + ": ";
    newText.innerHTML = content;

    newNick.style.color = 'red';
    newNick.style.fontWeight = "bold";
    newMessage.appendChild(newNick);
    newMessage.appendChild(newText);
    $("#chatBoxText").prepend(newMessage);
}

function sendMessage(user, nick, text) {
    let url = 'http://localhost:8000/chatroom/' + hashed_url
    let json = '{"user": "' + user + '", "nick": "' + nick + '", "message": "' + text + '"}';
    console.log(json);

    GM.xmlHttpRequest({
  method: "POST",
  url: url,
  data: json
 })
}

function addMessagesFromJson(jsonText) {
    let jsonObj = JSON.parse(jsonText);
    for (let i = 0; i < jsonObj.length; i++) {
        addMessage(jsonObj[i]['nick'], jsonObj[i]['message']);
        if (jsonObj[i]['id'] > biggestId) {
            biggestId = jsonObj[i]['id'];
        }
    }

    let url = 'http://localhost:8000/chatroom/' + hashed_url + '/' + biggestId
    console.log(url)

    GM.xmlHttpRequest({
  method: "GET",
  url: url,
  onload: function(response) {addMessagesFromJson(response.responseText);

    console.log([
      response.responseText
    ].join("\n"));
  }
});

    /*$.get(
      url,
      function(resp, textStatus, jqXHR) {console.log(resp); console.log(textStatus); addMessagesFromJson(resp)},
      'json'
    );*/
}

addMessagesFromJson('[]');