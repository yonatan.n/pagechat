create table messages
(
    id      INTEGER not null
        constraint messages_pk
            primary key autoincrement,
    room    TEXT,
    ts      REAL    not null,
    user    TEXT,
    nick    TEXT,
    message TEXT
);

create index room_id_ts
    on messages (room asc, id asc, ts desc);

