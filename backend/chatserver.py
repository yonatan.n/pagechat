import asyncio
import json
import traceback
from collections import defaultdict
from dataclasses import dataclass, asdict
from datetime import datetime, timezone, timedelta
from hashlib import sha256
from typing import List

import aiosqlite
from falcon.asgi import Request, Response

from backend.config import Config


@dataclass(frozen=True)
class Message:
    id: int
    user: str
    nick: str
    timestamp: float
    message: str

    def to_dict(self):
        return asdict(self)

    @staticmethod
    def from_dict(d, use_provided_timestamp: bool = False):
        return Message(d.get('id', 0),
                       d['user'],
                       d['nick'],
                       d['timestamp'] if use_provided_timestamp else datetime.now(tz=timezone.utc).timestamp(),
                       d['message']
                       )


def require_connection(f):
    async def wrapper(*args, **kwargs):
        if args[0].sql_con is None:
            await args[0].connect(args[0]._config.sql_path)
        return await f(args[0], *args[1:], **kwargs)
    return wrapper


class ChatServer:
    LOOKBACK = timedelta(minutes=10)

    def __init__(self, config: Config):
        self._config = config
        self.sql_con = None
        self.active = defaultdict(lambda: asyncio.Event())

    async def connect(self, path):
        self.sql_con = await aiosqlite.connect(path)
        self.sql_con.row_factory = aiosqlite.Row

    def get_key(self, url: str):
        h = sha256(url.encode('utf-8') + self._config.pepper).hexdigest()
        return h

    @require_connection
    async def get_messages(self, key: str, last_id: int) -> List[Message]:
        lookback_ts = (datetime.now(tz=timezone.utc) - ChatServer.LOOKBACK).timestamp()
        cursor = await self.sql_con.execute(
            "SELECT id, user, nick, ts, message FROM messages "
            "WHERE room = ? AND id > ? AND ts >= ? "
            "ORDER BY id DESC LIMIT 10",
            (key, last_id, lookback_ts)
        )

        rows = list(await cursor.fetchall())[::-1]
        if rows:
            return [Message(row['id'], row['user'], row['nick'], row['ts'], row['message']) for row in rows]
        else:
            try:
                await asyncio.wait_for(self.active[key].wait(), ChatServer.LOOKBACK.total_seconds() / 2)
                return await self.get_messages(key, last_id)
            except asyncio.exceptions.TimeoutError:
                return []


    @require_connection
    async def put_message(self, key: str, message: Message):
        cursor = await self.sql_con.execute(
            "INSERT INTO messages (room, user, nick, ts, message) VALUES (?, ?, ?, ?, ?)",
            (key, message.user, message.nick, message.timestamp, message.message)
        )
        await self.sql_con.commit()
        self.active[key].set()
        self.active[key].clear()
        return cursor.lastrowid

    async def on_get(self, req: Request, resp: Response, key: str, last_id: int = 0):
        #resp.text = "1"
        try:
            messages = await self.get_messages(key, last_id)
        except Exception as e:
            print("error", e)
            print(traceback.format_exc())
            raise e
        resp.text = json.dumps([message.to_dict() for message in messages])

    async def on_post(self, req: Request, resp: Response, key: str):
        print("doap")
        try:
            data = await req.stream.read()
            raw_data = json.loads(data)
            message = Message.from_dict(raw_data, False)
            resp.text = str(await self.put_message(key, message))
        except Exception as e:
            print("error", e)
            print(traceback.format_exc())
            raise e


