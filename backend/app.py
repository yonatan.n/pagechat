import falcon.asgi

from backend.chatserver import ChatServer
from backend.config import Config


def create_app(config=None):
    config = config or Config()
    chat_server = ChatServer(config)

    app = falcon.asgi.App()
    app.add_route('/chatroom/{key}', chat_server)
    app.add_route('/chatroom/{key}/{last_id:int}', chat_server)
    return app

app = create_app()
