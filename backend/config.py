import os
import pathlib
import uuid


class Config:
    DEFAULT_SQL_PATH = '/tmp/chatroom.sqlite3'
    DEFAULT_UUID_GENERATOR = uuid.uuid4
    DEFAULT_PEPPER = 'Q@W#R3akfwe-0aw3R#Aafb '

    def __init__(self):
        self.sql_path = pathlib.Path(
            os.environ.get('ASGI_CHATROOM_STORAGE_PATH', self.DEFAULT_SQL_PATH)
        )

        self.pepper = os.environ.get('ASGI_CHATROOM_PEPPER', self.DEFAULT_PEPPER)

        self.uuid_generator = Config.DEFAULT_UUID_GENERATOR